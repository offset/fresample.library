/*
** Copyright 2012 Dietrich Epp <depp@zdome.net>
** MorphOS port Copyright 2021 by Philippe Rimauro <offset@cpcscene.net>
*/

#ifndef FILE_H
#define FILE_H
#include <stddef.h>

struct file_data {
    void *data;
    size_t length;
};

void
file_init(struct file_data *fp);

int
file_read(struct file_data *fp, const char *path);

void
file_destroy(struct file_data *fp);

#endif
