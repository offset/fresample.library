/*
** Copyright 2012 Dietrich Epp <depp@zdome.net>
** MorphOS port Copyright 2021 by Philippe Rimauro <offset@cpcscene.net>
*/

#include "common.h"
#include "file.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MIN_ALLOC (256)

static int
file_read_std(struct file_data *fp, int fd, size_t sz)
{
    unsigned char *data;
    size_t pos, alloc, nalloc;
    ssize_t amt;

    alloc = sz + 1;
    if (alloc < MIN_ALLOC)
        alloc = MIN_ALLOC;
    data = xmalloc(alloc);
    if (!data)
        return 1;
    pos = 0;

    while (1) {
        if (pos >= alloc) {
            nalloc = alloc * 2;
            if (nalloc < alloc)
            {
                error("file too large");
                return 1;
            }
            data = xrealloc(data, nalloc);
            if (!data) {
                return 1;
            }
            alloc = nalloc;
        }
        amt = read(fd, data + pos, alloc - pos);
        if (amt > 0) {
            pos += amt;
        } else if (amt == 0) {
            break;
        } else {
            error("read failed");
            return 1;
        }
    }

    fp->data = data;
    fp->length = pos;

    return 0;
}

static int
file_read_fdes(struct file_data *fp, int fd)
{
    struct stat st;
    int r;
    r = fstat(fd, &st);
    if (r) {
        error("stat failed");
        return 1;
    }
    if (!S_ISREG(st.st_mode)) {
        file_read_std(fp, fd, 0);
        return 0;
    }
    if ((uint64_t) st.st_size > (size_t) -1) {
        error("file too large");
        return 1;
    }
    file_read_std(fp, fd, (size_t) st.st_size);

    return 0;
}

void
file_init(struct file_data *fp)
{
    fp->length = 0;
    fp->data = NULL;
}

int
file_read(struct file_data *fp, const char *path)
{
    int fd;
    if (!strcmp(path, "-")) {
        file_read_fdes(fp, STDIN_FILENO);
    } else {
        fd = open(path, O_RDONLY);
        if (fd < 0) {
            error("open failed");
            return 1;
        }
        file_read_fdes(fp, fd);
        close(fd);
    }

    return 0;
}

void
file_destroy(struct file_data *fp)
{
    if (!fp->data)
        return;
    free(fp->data);
    fp->data = NULL;
}
