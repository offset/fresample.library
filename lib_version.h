/*
   fresample.library version header file.
   Generated with LibMaker 0.11.
*/

#ifndef FRESAMPLE_LIB_VERSION_H
#define FRESAMPLE_LIB_VERSION_H

#define LIBNAME "fresample.library"
#define VERSION 2
#define REVISION 0
#define DATE "04.08.2021"

#define COPYRIGHT "� 2012 Dietrich Epp, MorphOS port � 2021 Philippe Rimauro"

#define VSTRING LIBNAME " " XSTR(VERSION) "." XSTR(REVISION) " (" DATE ") " COPYRIGHT
#define VERSTAG "\0$VER: " VSTRING

#define XSTR(s) STR(s)
#define STR(s) #s

#endif      /* FRESAMPLE_LIB_VERSION_H */
