/* Automatically generated header! Do not edit! */

#ifndef _PPCINLINE_FRESAMPLE_H
#define _PPCINLINE_FRESAMPLE_H

#ifndef __PPCINLINE_MACROS_H
#include <ppcinline/macros.h>
#endif /* !__PPCINLINE_MACROS_H */

#ifndef FRESAMPLE_BASE_NAME
#define FRESAMPLE_BASE_NAME FResampleBase
#endif /* !FRESAMPLE_BASE_NAME */

#define lfr_param_name(__p0) \
	(((const char *(*)(void *, lfr_param_t ))*(void**)((long)(FRESAMPLE_BASE_NAME) - 28))((void*)(FRESAMPLE_BASE_NAME), __p0))

#define lfr_param_lookup(__p0, __p1) \
	(((int (*)(void *, const char *, size_t ))*(void**)((long)(FRESAMPLE_BASE_NAME) - 34))((void*)(FRESAMPLE_BASE_NAME), __p0, __p1))

#define lfr_param_new() \
	(((struct lfr_param *(*)(void *))*(void**)((long)(FRESAMPLE_BASE_NAME) - 40))((void*)(FRESAMPLE_BASE_NAME)))

#define lfr_param_free(__p0) \
	(((void (*)(void *, struct lfr_param *))*(void**)((long)(FRESAMPLE_BASE_NAME) - 46))((void*)(FRESAMPLE_BASE_NAME), __p0))

#define lfr_param_copy(__p0) \
	(((struct lfr_param *(*)(void *, struct lfr_param *))*(void**)((long)(FRESAMPLE_BASE_NAME) - 52))((void*)(FRESAMPLE_BASE_NAME), __p0))

#define lfr_param_seti(__p0, __p1, __p2) \
	(((void (*)(void *, struct lfr_param *, lfr_param_t , int ))*(void**)((long)(FRESAMPLE_BASE_NAME) - 58))((void*)(FRESAMPLE_BASE_NAME), __p0, __p1, __p2))

#define lfr_param_setf(__p0, __p1, __p2) \
	(((void (*)(void *, struct lfr_param *, lfr_param_t , double ))*(void**)((long)(FRESAMPLE_BASE_NAME) - 64))((void*)(FRESAMPLE_BASE_NAME), __p0, __p1, __p2))

#define lfr_param_geti(__p0, __p1, __p2) \
	(((void (*)(void *, struct lfr_param *, lfr_param_t , int *))*(void**)((long)(FRESAMPLE_BASE_NAME) - 70))((void*)(FRESAMPLE_BASE_NAME), __p0, __p1, __p2))

#define lfr_param_getf(__p0, __p1, __p2) \
	(((void (*)(void *, struct lfr_param *, lfr_param_t , double *))*(void**)((long)(FRESAMPLE_BASE_NAME) - 76))((void*)(FRESAMPLE_BASE_NAME), __p0, __p1, __p2))

#define lfr_info_name(__p0) \
	(((const char *(*)(void *, int ))*(void**)((long)(FRESAMPLE_BASE_NAME) - 82))((void*)(FRESAMPLE_BASE_NAME), __p0))

#define lfr_info_lookup(__p0, __p1) \
	(((int (*)(void *, const char *, size_t ))*(void**)((long)(FRESAMPLE_BASE_NAME) - 88))((void*)(FRESAMPLE_BASE_NAME), __p0, __p1))

#define lfr_filter_new(__p0, __p1) \
	(((void (*)(void *, struct lfr_filter **, struct lfr_param *))*(void**)((long)(FRESAMPLE_BASE_NAME) - 94))((void*)(FRESAMPLE_BASE_NAME), __p0, __p1))

#define lfr_filter_free(__p0) \
	(((void (*)(void *, struct lfr_filter *))*(void**)((long)(FRESAMPLE_BASE_NAME) - 100))((void*)(FRESAMPLE_BASE_NAME), __p0))

#define lfr_filter_delay(__p0) \
	(((lfr_fixed_t (*)(void *, const struct lfr_filter *))*(void**)((long)(FRESAMPLE_BASE_NAME) - 106))((void*)(FRESAMPLE_BASE_NAME), __p0))

#define lfr_filter_geti(__p0, __p1, __p2) \
	(((void (*)(void *, const struct lfr_filter *, int , int *))*(void**)((long)(FRESAMPLE_BASE_NAME) - 112))((void*)(FRESAMPLE_BASE_NAME), __p0, __p1, __p2))

#define lfr_filter_getf(__p0, __p1, __p2) \
	(((void (*)(void *, const struct lfr_filter *, int , double *))*(void**)((long)(FRESAMPLE_BASE_NAME) - 118))((void*)(FRESAMPLE_BASE_NAME), __p0, __p1, __p2))

#define lfr_resample(__p0, __p1, __p2, __p3, __p4, __p5, __p6, __p7, __p8, __p9, __p10) \
	(((void (*)(void *, lfr_fixed_t *, lfr_fixed_t , unsigned *, int , void *, lfr_fmt_t , int , const void *, lfr_fmt_t , int , const struct lfr_filter *))*(void**)((long)(FRESAMPLE_BASE_NAME) - 124))((void*)(FRESAMPLE_BASE_NAME), __p0, __p1, __p2, __p3, __p4, __p5, __p6, __p7, __p8, __p9, __p10))

#define lfr_getcpufeatures() \
	(((const struct lfr_cpuf *(*)(void *))*(void**)((long)(FRESAMPLE_BASE_NAME) - 130))((void*)(FRESAMPLE_BASE_NAME)))

#define lfr_setcpufeatures(__p0) \
	(((unsigned (*)(void *, unsigned ))*(void**)((long)(FRESAMPLE_BASE_NAME) - 136))((void*)(FRESAMPLE_BASE_NAME), __p0))

#define lfr_swap16(__p0, __p1, __p2) \
	(((void (*)(void *, void *, const void *, size_t ))*(void**)((long)(FRESAMPLE_BASE_NAME) - 142))((void*)(FRESAMPLE_BASE_NAME), __p0, __p1, __p2))

#endif /* !_PPCINLINE_FRESAMPLE_H */
