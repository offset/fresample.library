#ifndef CLIB_FRESAMPLE_PROTOS_H
#define CLIB_FRESAMPLE_PROTOS_H

/*
   fresample.library C prototypes
   Copyright � � 2012 Dietrich Epp <depp@zdome.net>
   Generated with LibMaker 0.11.
*/


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

const char * lfr_param_name(lfr_param_t);
int lfr_param_lookup(const char *, size_t);
struct lfr_param * lfr_param_new(VOID);
void lfr_param_free(struct lfr_param *);
struct lfr_param * lfr_param_copy(struct lfr_param *);
void lfr_param_seti(struct lfr_param *, lfr_param_t, int);
void lfr_param_setf(struct lfr_param *, lfr_param_t, double);
void lfr_param_geti(struct lfr_param *, lfr_param_t, int *);
void lfr_param_getf(struct lfr_param *, lfr_param_t, double *);
const char * lfr_info_name(int);
int lfr_info_lookup(const char *, size_t);
void lfr_filter_new(struct lfr_filter **, struct lfr_param *);
void lfr_filter_free(struct lfr_filter *);
lfr_fixed_t lfr_filter_delay(const struct lfr_filter *);
void lfr_filter_geti(const struct lfr_filter *, int, int *);
void lfr_filter_getf(const struct lfr_filter *, int, double *);
void lfr_resample(lfr_fixed_t *, lfr_fixed_t, unsigned *, int, void *, lfr_fmt_t, int, const void *, lfr_fmt_t, int, const struct lfr_filter *);
const struct lfr_cpuf * lfr_getcpufeatures(VOID);
unsigned lfr_setcpufeatures(unsigned);
void lfr_swap16(void *, const void *, size_t);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CLIB_FRESAMPLE_PROTOS_H */
