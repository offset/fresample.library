/* Automatically generated header! Do not edit! */

#ifndef PROTO_FRESAMPLE_H
#define PROTO_FRESAMPLE_H

#ifndef __NOLIBBASE__
extern struct Library *
#ifdef __CONSTLIBBASEDECL__
__CONSTLIBBASEDECL__
#endif /* __CONSTLIBBASEDECL__ */
FResampleBase;
#endif /* !__NOLIBBASE__ */

#include <libraries/fresample.h>

#ifdef __GNUC__
#ifdef __PPC__
#ifndef _NO_PPCINLINE
#include <ppcinline/fresample.h>
#endif /* _NO_PPCINLINE */
#else
#ifndef _NO_INLINE
#include <inline/fresample.h>
#endif /* _NO_INLINE */
#endif /* __PPC__ */
#elif defined(__VBCC__)
#include <inline/fresample_protos.h>
#else
#include <pragmas/fresample_pragmas.h>
#endif /* __GNUC__ */

#endif /* !PROTO_FRESAMPLE_H */

