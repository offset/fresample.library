/*
    fresample.library definitions

    Copyright � 2021 Philippe Rimauro. All rights reserved.
*/

#ifndef LIBRARIES_FRESAMPLE_H
#define LIBRARIES_FRESAMPLE_H

/* Copyright 2012 Dietrich Epp <depp@zdome.net> */

#ifdef __cplusplus
extern "C" {
#endif

#if defined(_MSC_VER)
# define LFR_INT64 __int64
#elif defined(__GNUC__)
# define LFR_INT64 long long
#endif

#if defined(_M_X64) || defined(__x86_64__)
# define LFR_CPU_X64 1
# define LFR_CPU_X86 1
#elif defined(_M_IX86) || defined(__i386__)
# define LFR_CPU_X86 1
#elif defined(__ppc64__)
# define LFR_CPU_PPC64 1
# define LFR_CPU_PPC 1
#elif defined(__ppc__)
# define LFR_CPU_PPC 1
#endif

#define LFR_LITTLE_ENDIAN 1234
#define LFR_BIG_ENDIAN 4321

#if defined(__BYTE_ORDER__)
# if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#  define LFR_BYTE_ORDER LFR_BIG_ENDIAN
# elif __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#  define LFR_BYTE_ORDER LFR_LITTLE_ENDIAN
# endif
#elif defined(__BIG_ENDIAN__)
# define LFR_BYTE_ORDER LFR_BIG_ENDIAN
#elif defined(__LITTLE_ENDIAN__)
# define LFR_BYTE_ORDER LFR_LITTLE_ENDIAN
#endif

#if !defined(LFR_BYTE_ORDER)
# if defined(LFR_CPU_X86)
#  define LFR_BYTE_ORDER LFR_LITTLE_ENDIAN
# elif defined(LFR_CPU_PPC)
#  define LFR_BYTE_ORDER LFR_BIG_ENDIAN
# else
#  error "cannot determine machine byte order"
# endif
#endif

/* ========================================
   CPU features
   ======================================== */

/*
  CPU features to use or disable.
*/
enum {
#if defined(LFR_CPU_X86)
    LFR_CPUF_MMX     = (1u << 0),
    LFR_CPUF_SSE     = (1u << 1),
    LFR_CPUF_SSE2    = (1u << 2),
    LFR_CPUF_SSE3    = (1u << 3),
    LFR_CPUF_SSSE3   = (1u << 4),
    LFR_CPUF_SSE4_1  = (1u << 5),
    LFR_CPUF_SSE4_2  = (1u << 6),
#else
    LFR_CPUF_MMX     = 0u,
    LFR_CPUF_SSE     = 0u,
    LFR_CPUF_SSE2    = 0u,
    LFR_CPUF_SSE3    = 0u,
    LFR_CPUF_SSSE3   = 0u,
    LFR_CPUF_SSE4_1  = 0u,
    LFR_CPUF_SSE4_2  = 0u,
#endif

#if defined(LFR_CPU_PPC)
    LFR_CPUF_ALTIVEC = (1u << 0),
#else
    LFR_CPUF_ALTIVEC = 0u,
#endif

    LFR_CPUF_NONE = 0u,
    LFR_CPUF_ALL = 0xffffffffu
};

/*
  Information about a CPU flag.
*/
struct lfr_cpuf {
    char name[8];
    unsigned flag;
};

/* ========================================
   Sample formats
   ======================================== */

/*
  Audio sample formats.
*/
typedef enum {
    LFR_FMT_U8,
    LFR_FMT_S16BE,
    LFR_FMT_S16LE,
    LFR_FMT_S24BE,
    LFR_FMT_S24LE,
    LFR_FMT_F32BE,
    LFR_FMT_F32LE
} lfr_fmt_t;

#define LFR_FMT_COUNT ((int) LFR_FMT_F32LE + 1)

#if LFR_BYTE_ORDER == LFR_BIG_ENDIAN
# define LFR_FMT_S16_NATIVE LFR_FMT_S16BE
# define LFR_FMT_S16_SWAPPED LFR_FMT_S16LE
# define LFR_FMT_F32_NATIVE LFR_FMT_F32BE
# define LFR_FMT_F32_SWAPPED LFR_FMT_F32LE
#else
# define LFR_FMT_S16_NATIVE LFR_FMT_S16LE
# define LFR_FMT_S16_SWAPPED LFR_FMT_S16BE
# define LFR_FMT_F32_NATIVE LFR_FMT_F32LE
# define LFR_FMT_F32_SWAPPED LFR_FMT_F32BE
#endif

/* ========================================
   Resampling parameters
   ======================================== */

/*
  Names for filter quality presets.
*/
enum {
    /*
      Low quality: Currently, quality 0..3 are identical, since
      further reductions in quality do not increase performance.
    */
    LFR_QUALITY_LOW = 2,

    /*
      Medium quality: Transition band of 23%, nominal attenuation of
      60 dB.  Actual attenuation may be higher.
    */
    LFR_QUALITY_MEDIUM = 5,

    /*
      High quality: Transition band of 10%, nominal attenuation of 96
      dB.  It is not normally reasonable to increase quality beyond
      this level unless you are competing for the prettiest
      spectrogram.
    */
    LFR_QUALITY_HIGH = 8,

    /*
      Ultra quality: Transition band of 3%, nominal attenuation of 120
      dB.  Filter coefficients may not fit in L2 cache.  Impulse
      response may be several milliseconds long.
    */
    LFR_QUALITY_ULTRA = 10
};

/*
  Parameters for the filter generator.

  Filter generation goes through two stages.

  1. In the first stage, the resampling parameters are used to create
  a filter specification.  The filter specification consists of
  normalized frequencies for the pass band, stop band, and stop band
  attenuation.  This stage uses simple logic to create a filter
  specification that "makes sense" for any input.  It relaxes the
  filter specification for ultrasonic (inaudible) frequencies and
  ensures that enough of the input signal passes through.

  2. In the second stage, an FIR filter is generated that fits the
  filter specified by the first stage.

  Normally, for resampling, you will specify QUALITY, INRATE, and
  OUTRATE.  A filter specification for the conversion will be
  automatically generated with the given subjective quality level.

  If you are a signal-processing guru, you can create the filter
  specification directly by setting FPASS, FSTOP, and ATTEN.
*/
typedef enum {
    /*
      High level filter parameters.  These are typically the only
      parameters you will need to set.
    */

    /* Filter quality, default 8.  An integer between 0 and 10 which
       determines the default values for other parameters.  */
    LFR_PARAM_QUALITY,

    /* Input sample rate, in Hz.  The default is -1, which creates a
       generic filter.  */
    LFR_PARAM_INRATE,

    /* Output sample rate.  If the input sample rate is specified,
       then this is measured in Hz.  Otherwise, if the input rate is
       -1, then this value is relative to the input sample rate (so
       you would use 0.5 for downsampling by a factor of two).
       Defaults to the same value as the input sample rate, which
       creates a filter which can be used for upsampling at any ratio.
       Note that increasing the output rate above the input rate has
       no effect, all upsampling filters for a given input frequency
       are identical.  */
    LFR_PARAM_OUTRATE,

    /*
      Medium level filter parameters.  These parameters affect how the
      filter specification is generated from the input and output
      sample rates.  Most of these parameters have default values
      which depend on the QUALITY setting.
    */

    /* The width of the filter transition band, as a fraction of the
       input sample rate.  This value will be enlarged to extend the
       transition band to MAXFREQ and will be narrowed to extend the
       pass band to MINBW.  The default value depends on the QUALITY
       setting, and gets narrower as QUALITY increases.  */
    LFR_PARAM_FTRANSITION,

    /* Maximum audible frequency.  The pass band will be narrowed to
       fit within the range of audible frequencies.  Default value is
       20 kHz if the input frequency is set, otherwise this parameter
       is unused.  If you want to preserve ultrasonic frequencies,
       disable this parameter by setting it to -1.  This is disabled
       by default at absurd quality settings (9 and 10).  */
    LFR_PARAM_MAXFREQ,

    /* A flag which allows aliasing noise as long as it is above
       MAXFREQ.  This flag improves the subjective quality of
       low-quality filters by increasing their bandwidth, but causes
       problems for high-quality filters by increasing noise.  Default
       value depends on QUALITY setting, and is set for low QUALITY
       values.  Has no effect if MAXFREQ is disabled.  */
    LFR_PARAM_LOOSE,

    /* Minimum size of pass band, as a fraction of the output
       bandwidth.  This prevents the filter designer from filtering
       out the entire signal, which can happen when downsampling by a
       large enough ratio.  The default value is 0.5 at low quality
       settings, and higher at high quality settings.  The filter size
       can increase dramatically as this number approaches 1.0.  */
    LFR_PARAM_MINFPASS,

    /*
      Filter specification.  These parameters are normally generated
      from the higher level parameters.  If the filter specification
      is set, then the higher level parameters will all be ignored.
    */

    /* The end of the pass band, as a fraction of the input sample
       rate.  Normally, the filter designer chooses this value.  */
    LFR_PARAM_FPASS,

    /* The start of the stop band, as a fraction of the input sample
       rate.  Normally, the filter designer chooses this value.  */
    LFR_PARAM_FSTOP,

    /* Desired stop band attenuation, in dB.  Larger numbers are
       increase filter quality.  Default value depends on the QUALITY
       setting.  */
    LFR_PARAM_ATTEN
} lfr_param_t;

#define LFR_PARAM_COUNT ((int) LFR_PARAM_ATTEN + 1)

/*
  A set of filter parameters.
*/
struct lfr_param;

/* ========================================
   Resampling
   ======================================== */

/*
  Information queries that a filter responds to.  Each query gives an
  integer or floating point result, automatically cast to the type
  requested.
*/
enum {
    /*
      The size of the filter, also known as the filter's order.  This
      does not include oversampling.  The resampler will read this
      many samples, starting with the current position (rounded down),
      whenever it calculates a sample.

      In other words, this is the amount of overlap needed when
      resampling consecutive buffers.
    */
    LFR_INFO_SIZE,

    /*
      Filter delay.  Note that lfr_filter_delay returns a fixed point
      number and is usually preferable.
    */
    LFR_INFO_DELAY,

    /*
      The is the number of bytes used by filter coefficients.
    */
    LFR_INFO_MEMSIZE,

    /*
      The normalized pass frequency the filter was designed with.
    */
    LFR_INFO_FPASS,

    /*
      The normalized stop frequency the filter was designed with.
    */
    LFR_INFO_FSTOP,

    /*
      The stopband attenuation, in dB, that the filter was designed
      with.
    */
    LFR_INFO_ATTEN
};

#define LFR_INFO_COUNT (LFR_INFO_ATTEN + 1)

/*
  A 32.32 fixed point number.  This is used for expressing fractional
  positions in a buffer.

  When used for converting from sample rate f_s to f_d, the timing
  error at time t is bounded by t * 2^-33 * r_d / r_s.  For a
  pessimistic conversion ratio, 8 kHz -> 192 kHz, this means that it
  will take at least five days to accumulate one millisecond of error.
*/
typedef LFR_INT64 lfr_fixed_t;

/*
  A filter for resampling audio.
*/
struct lfr_filter;

#ifdef __cplusplus
}
#endif

#endif      /* LIBRARIES_FRESAMPLE_H */
