/*
   fresample.library
   Generated with LibMaker 0.11.
*/

/****** fresample.library/background ****************************************
*
* DESCRIPTION
*   Fast, free sample rate conversion
*
*   LibFResample is a library for resampling audio with a permissive
*   (FreeBSD style) license.  Like other audio resampling libraries, it is
*   based on the bandlimited interpolation algorithm described by Julius
*   Orion Smith.  LibFResample is designed to use SIMD operations where
*   available.  Currently, LibFResample supports SSE2 and AltiVec.
*
*   If you can hook me up with some hardware, I may be able to optimize
*   this library for other architectures - ARM NEON, Cell SPE, et cetera.
*
*   Resampling speed
*   ----------------
*
*   LibFResample gets its speed by precalculating filter coefficients for
*   the desired conversion ratio, reorganizing coefficients for maximum
*   cache locality, and using SIMD operations as much as possible.
*
*   System A: 1.6 GHz Intel Atom, GCC 4.4 (circa 2008)
*   System B: 2.0 GHz IBM PowerPC G5, GCC 4.0 (circa 2005)
*   System C: 3.2 GHz AMD Phenom II, GCC 4.7 (circa 2010)
*   System D: 1.7 GHz Intel Core i5, Clang 4.0 (circa 2012)
*
*   Task: Resample 16-bit stereo audio from 48 kHz to 44.1 kHz
*   Speed: 1x is realtime, 2x is twice realtime, etc.
*   SRC: Secret Rabbit Code 0.1.8, also known as libsamplerate
*
*       Settings    System A    System B    System C    System D
*
*       LibFResample
*       Q=5 Medium  197x        545x        1276x       1247x
*       Q=8 High    47x         141x        318x        303x
*       Q=10 Ultra  13x         51x         107x        87x
*
*       Secret Rabbit Code
*       Sinc Fast   13x         20x         68x         77x
*       Sinc Med.   5.9x        9.3x        35x         39x
*       Sinc Best   0.69x       1.9x        8.5x        11x
*
*   That's fast!  To put it in perspective, System D, an entry level
*   MacBook Air from 2012, can resample an hour of audio in under 3
*   seconds at medium quality (Q=5).  In fact, LibFResample is so fast, it
*   even beats Secret Rabbit Code's linear interpolator, which the docs
*   note is "blindingly fast"...
*
*   Task: Resample 16-bit mono audio from 48 kHz to 44.1 kHz
*
*       Settings    System A    System B    System C    System D
*
*       LibFResample
*       Q=2 Low     484x        887x        2759x       3195x
*       Q=5 Medium  286x        721x        1875x       1811x
*
*       Secret Rabbit Code
*       ZOH         119x        250x        986x        1326x
*       Linear      113x        242x        895x        1112x
*
*   Notes: SRC throughput figures were calculated by dividing the output
*   of SRC's throughput tests by 44100.  LibFResample throughput figures
*   were calculated by resampling two minutes of pink noise twenty times
*   after warming the cache.
*
*   The 'benchmark.py' script in the tests folder will benchmark
*   LibFResample, it requires SoX.
*
*   Audio quality
*   -------------
*
*   LibFResample uses band-limited interpolation and dithering to achieve
*   high-quality output.  The filter is a simple windowed sinc filter with
*   a Kaiser window, the window size and beta parameter are adjusted to
*   achieve the desired SNR and transition band.  There are better ways to
*   design filters, but this works.
*
*   An included test script finds the bandwidth and signal to noise ratio
*   of the resampler at various quality settings.  The bandwidth and SNR
*   will vary depending on the exact sample rates used.  In particular,
*   the bandwidth decreases if the input sample rate increases -- but if
*   you record 96 kHz or 192 kHz audio, you probably want to use higher
*   quality settings anyway.
*
*       Settings        Bandwidth   SNR
*
*       Q=2  Low        13.3 kHz    29 dB
*       Q=5  Medium     16.3 kHz    84 dB
*       Q=8  High       19.5 kHz    92 dB
*       Q=10 Ultra      21.2 kHz    92 dB
*
*   Proper dithering introduces a noise floor of 1 ULP peak-to-peak, which
*   at 16 bit resolution puts the noise floor at -96.  The test dithers
*   both input and output, giving a noise floor of -93 dB.  At high
*   quality settings, the measured SNR of 92 dB is nearly perfect.
*
*   Bandwidth is measured by doing a binary search to find the 3 dB
*   attenuation point.  The attenuation is measured by resampling windowed
*   sine waves.
*
*   The SNR is measured by repeatedly resampling sine waves and taking the
*   FFT of the resampled result.  The original sine wave is zeroed from
*   the FFT bins and the signal power in the other bins is computed.  Sine
*   waves at 40 different frequencies are tested and the worst SNR is
*   recorded.
*
*   The 'quality.py' script in the tests folder will compute these quality
*   figures, it requires SciPy.
*
*   Theory of operation
*   -------------------
*
*   Resampling audio takes two steps: first you design a low-pass filter,
*   then you use the filter to resample the audio.  In general, a new
*   filter must be created for each combination of sample rates, filter
*   parameters, and bit depth.
*
*   The low-pass filter is used to remove aliasing from the resampled
*   output -- aliasing is undesirable noise created during the resampling
*   process.  Adjusting the filter parameters is the only way to change
*   the quality of the audio output.  There are two major parameters to
*   filter design:
*
*   1. Signal to noise ratio.  A filter with a higher SNR more efficiently
*   excludes aliasing noise, but increasing the SNR also makes the filter
*   larger.  Note that the noise is not pleasant noise like white noise;
*   the noise has a gritty, low-fi sound.
*
*   2. Bandwidth.  A filter with a higher bandwidth includes more of the
*   original signal, but increasing the bandwidth also makes the filter
*   larger.  (The crucial parameter is actually transition bandwidth,
*   between the filter's pass band and stop band.  Decreasing the
*   transition bandwidth increases the filter size.)
*
*   LibFResample includes a function which creates a filter with the
*   desired SNR and transition bandwidth, and there are also some preset
*   SNR and transition bandwidth combinations.  The function also performs
*   a number of adjustments of the design parameters to ensure that the
*   pass band is large enough and that the filter isn't overdesigned to
*   preserve ultrasonic frequencies.
*
*   Internal details
*   ----------------
*
*   The filter is a simple sinc filter windowed with the Kaiser window.
*   Choosing the parameter for the Kaiser window allows us to adjust the
*   level of the side lobes, and higher side lobes contribute to aliasing
*   noise.  Increasing the filter size decreases the width of the window's
*   main lobe, which decreases the transition bandwidth in the resulting
*   filter.
*
*   Many copies of the filter are generated, each with a fractional (less
*   than one sample) offset from each other.  Choosing a copy with a given
*   offset allows us to sample the original audio at that offset.
*   Interpolating between copies allows us to sample the audio at finer
*   intervals without a significant increase in memory usage.
*
*   Filters may be created with 16-bit integer or single precision
*   floating point coefficients.  The cutoff is typically above Q=5.  As
*   filter sizes increase, coefficient quantization begins to dominate
*   stopband attenuation.
*
*   At low quality settings, the fractional copies are individually
*   normalized.  Otherwise, variations in DC gain will modulate the input
*   signal.
*
* LICENSE
*   Copyright 2012 - 2013 Dietrich Epp <depp@zdome.net>
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are
*   met:
*
*      1. Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*
*      2. Redistributions in binary form must reproduce the above
*         copyright notice, this list of conditions and the following
*         disclaimer in the documentation and/or other materials provided
*         with the distribution.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
*   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* HISTORY
*
*****************************************************************************
*
*/

#define __NOLIBBASE__

#include <proto/exec.h>
#include <exec/resident.h>
#include <exec/libraries.h>
#include <libraries/query.h>
#include <clib/alib_protos.h>
#include <clib/debug_protos.h>

#define UNUSED __attribute__((unused))

#include "lib_version.h"
#include "library.h"
#include "lib/fresample.h"

const char LibName[] = LIBNAME;
extern const char VTag[];

struct Library *SysBase;


struct Library *LibInit(struct Library *unused, APTR seglist, struct Library *sysb);
struct MyLibBase *lib_init(struct MyLibBase *base, APTR seglist, struct Library *SysBase);
APTR lib_expunge(struct MyLibBase *base);
struct Library *LibOpen(void);
ULONG LibClose(void);
APTR LibExpunge(void);
ULONG LibReserved(void);
BOOL InitResources(struct MyLibBase *base);
VOID FreeResources(struct MyLibBase *base);

const char * Liblfr_param_name(struct MyLibBase *base, lfr_param_t pname);
int Liblfr_param_lookup(struct MyLibBase *base, const char *pname, size_t len);
struct lfr_param * Liblfr_param_new(struct MyLibBase *base);
void Liblfr_param_free(struct MyLibBase *base, struct lfr_param *param);
struct lfr_param * Liblfr_param_copy(struct MyLibBase *base, struct lfr_param *param);
void Liblfr_param_seti(struct MyLibBase *base, struct lfr_param *param, lfr_param_t pname, int value);
void Liblfr_param_setf(struct MyLibBase *base, struct lfr_param *param, lfr_param_t pname, double value);
void Liblfr_param_geti(struct MyLibBase *base, struct lfr_param *param, lfr_param_t pname, int *value);
void Liblfr_param_getf(struct MyLibBase *base, struct lfr_param *param, lfr_param_t pname, double *value);
const char * Liblfr_info_name(struct MyLibBase *base, int pname);
int Liblfr_info_lookup(struct MyLibBase *base, const char *pname, size_t len);
void Liblfr_filter_new(struct MyLibBase *base, struct lfr_filter **fpp, struct lfr_param *param);
void Liblfr_filter_free(struct MyLibBase *base, struct lfr_filter *fp);
lfr_fixed_t Liblfr_filter_delay(struct MyLibBase *base, const struct lfr_filter *fp);
void Liblfr_filter_geti(struct MyLibBase *base, const struct lfr_filter *fp, int iname, int *value);
void Liblfr_filter_getf(struct MyLibBase *base, const struct lfr_filter *fp, int iname, double *value);
void Liblfr_resample(struct MyLibBase *base, lfr_fixed_t *pos, lfr_fixed_t inv_ratio, unsigned *dither, int nchan, void *out, lfr_fmt_t outfmt, int outlen, const void *in, lfr_fmt_t infmt, int inlen, const struct lfr_filter *filter);
const struct lfr_cpuf * Liblfr_getcpufeatures(struct MyLibBase *base);
unsigned Liblfr_setcpufeatures(struct MyLibBase *base, unsigned flags);
void Liblfr_swap16(struct MyLibBase *base, void * dest, const void * src, size_t count);


BOOL InitResources(UNUSED struct MyLibBase *base)
{
	return TRUE;
}


VOID FreeResources(UNUSED struct MyLibBase *base)
{
}


const struct TagItem RTags[] =
{
	{ QUERYINFOATTR_NAME, (IPTR)LibName },
	{ QUERYINFOATTR_IDSTRING, (IPTR)&VTag[1] },
	{ QUERYINFOATTR_COPYRIGHT, (IPTR)COPYRIGHT },
	{ QUERYINFOATTR_DATE, (IPTR)DATE },
	{ QUERYINFOATTR_VERSION, VERSION },
	{ QUERYINFOATTR_REVISION, REVISION },
	{ QUERYINFOATTR_SUBTYPE, QUERYSUBTYPE_LIBRARY },
	{ TAG_END,  0 }
};

struct Resident ROMTag =
{
	RTC_MATCHWORD,
	&ROMTag,
	&ROMTag + 1,
	RTF_EXTENDED | RTF_PPC,
	VERSION,
	NT_LIBRARY,
	0,
	(char *)LibName,
	VSTRING,
	(APTR)LibInit,
	REVISION,
	(struct TagItem *)RTags
};


APTR JumpTable[] =
{
	(APTR)FUNCARRAY_BEGIN,
	(APTR)FUNCARRAY_32BIT_NATIVE,
	(APTR)LibOpen,
	(APTR)LibClose,
	(APTR)LibExpunge,
	(APTR)LibReserved,
	(APTR)0xFFFFFFFF,
	(APTR)FUNCARRAY_32BIT_SYSTEMV,
	(APTR)Liblfr_param_name,
	(APTR)Liblfr_param_lookup,
	(APTR)Liblfr_param_new,
	(APTR)Liblfr_param_free,
	(APTR)Liblfr_param_copy,
	(APTR)Liblfr_param_seti,
	(APTR)Liblfr_param_setf,
	(APTR)Liblfr_param_geti,
	(APTR)Liblfr_param_getf,
	(APTR)Liblfr_info_name,
	(APTR)Liblfr_info_lookup,
	(APTR)Liblfr_filter_new,
	(APTR)Liblfr_filter_free,
	(APTR)Liblfr_filter_delay,
	(APTR)Liblfr_filter_geti,
	(APTR)Liblfr_filter_getf,
	(APTR)Liblfr_resample,
	(APTR)Liblfr_getcpufeatures,
	(APTR)Liblfr_setcpufeatures,
	(APTR)Liblfr_swap16,
	(APTR)0xFFFFFFFF,
	(APTR)FUNCARRAY_END
};


struct MyLibBase * lib_init(struct MyLibBase *base, APTR seglist, UNUSED struct Library *sysbase)
{
	InitSemaphore(&base->BaseLock);
	base->Seglist = seglist;
	return base;
}

struct TagItem LibTags[] = {
	{ LIBTAG_FUNCTIONINIT, (IPTR)JumpTable },
	{ LIBTAG_LIBRARYINIT,  (IPTR)lib_init },
	{ LIBTAG_MACHINE,      MACHINE_PPC },
	{ LIBTAG_BASESIZE,     sizeof(struct MyLibBase) },
	{ LIBTAG_SEGLIST,      0 },
	{ LIBTAG_TYPE,         NT_LIBRARY },
	{ LIBTAG_NAME,         0 },
	{ LIBTAG_IDSTRING,     0 },
	{ LIBTAG_FLAGS,        LIBF_CHANGED | LIBF_SUMUSED },
	{ LIBTAG_VERSION,      VERSION },
	{ LIBTAG_REVISION,     REVISION },
	{ LIBTAG_PUBLIC,       TRUE },
	{ TAG_END,             0 },
};

struct Library * LibInit(UNUSED struct Library *unused, APTR seglist, struct Library *sysbase)
{
	SysBase = sysbase;

	LibTags[4].ti_Data = (IPTR)seglist;
	LibTags[6].ti_Data = (IPTR)ROMTag.rt_Name;
	LibTags[7].ti_Data = (IPTR)ROMTag.rt_IdString;

	return (NewCreateLibrary(LibTags));
}


struct Library * LibOpen(void)
{
	struct MyLibBase *base = (struct MyLibBase*)REG_A6;
	struct Library *lib = (struct Library*)base;

	ObtainSemaphore(&base->BaseLock);

	if (!base->InitFlag)
	{
		if (InitResources(base)) base->InitFlag = TRUE;
		else
		{
			FreeResources(base);
			lib = NULL;
		}
	}

	if (lib)
	{
		base->LibNode.lib_Flags &= ~LIBF_DELEXP;
		base->LibNode.lib_OpenCnt++;
	}

	ReleaseSemaphore(&base->BaseLock);
	if (!lib) lib_expunge(base);
	return lib;
}


ULONG LibClose(void)
{
	struct MyLibBase *base = (struct MyLibBase*)REG_A6;
	ULONG ret = 0;

	ObtainSemaphore(&base->BaseLock);

	if (--base->LibNode.lib_OpenCnt == 0)
	{
		if (base->LibNode.lib_Flags & LIBF_DELEXP) ret = (ULONG)lib_expunge(base);
	}

	if (ret == 0) ReleaseSemaphore(&base->BaseLock);
	return ret;
}


APTR LibExpunge(void)
{
	struct MyLibBase *base = (struct MyLibBase*)REG_A6;

	return(lib_expunge(base));
}


APTR lib_expunge(struct MyLibBase *base)
{
	APTR seglist = NULL;

	ObtainSemaphore(&base->BaseLock);

	if (base->LibNode.lib_OpenCnt == 0)
	{
		FreeResources(base);
		Forbid();
		Remove((struct Node*)base);
		Permit();
		seglist = base->Seglist;
		FreeMem((UBYTE*)base - base->LibNode.lib_NegSize, base->LibNode.lib_NegSize + base->LibNode.lib_PosSize);
		base = NULL;    /* freed memory, no more valid */
	}
	else base->LibNode.lib_Flags |= LIBF_DELEXP;

	if (base) ReleaseSemaphore(&base->BaseLock);
	return seglist;
}


ULONG LibReserved(void)
{
	return 0;
}

/****** fresample.library/lfr_param_name ************************************
*
* NAME
*   lfr_param_name -- {short} (V1)
*
* SYNOPSIS
*   const char * lfr_param_name(lfr_param_t pname)
*
* FUNCTION
*   Get the name of a parameter, or return NULL if the paramater does
*   not exist.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

const char * Liblfr_param_name(UNUSED struct MyLibBase *base, lfr_param_t pname)
{
    return lfr_param_name(pname);
}



/****** fresample.library/lfr_param_lookup **********************************
*
* NAME
*   lfr_param_lookup -- {short} (V1)
*
* SYNOPSIS
*   int lfr_param_lookup(const char *pname, size_t len)
*
* FUNCTION
*   Get the index of a parameter by name, or return -1 if the parameter
*   does not exist.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

int Liblfr_param_lookup(UNUSED struct MyLibBase *base, const char *pname, size_t len)
{
    return lfr_param_lookup(pname, len);
}



/****** fresample.library/lfr_param_new *************************************
*
* NAME
*   lfr_param_new -- {short} (V1)
*
* SYNOPSIS
*   struct lfr_param * lfr_param_new(VOID)
*
* FUNCTION
*   Create a new filter parameter set.  Returns NULL if out of memory.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

struct lfr_param * Liblfr_param_new(UNUSED struct MyLibBase *base)
{
    return lfr_param_new();
}



/****** fresample.library/lfr_param_free ************************************
*
* NAME
*   lfr_param_free -- {short} (V1)
*
* SYNOPSIS
*   void lfr_param_free(struct lfr_param *param)
*
* FUNCTION
*   Free a filter parameter set.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

void Liblfr_param_free(UNUSED struct MyLibBase *base, struct lfr_param *param)
{
    lfr_param_free(param);
}



/****** fresample.library/lfr_param_copy ************************************
*
* NAME
*   lfr_param_copy -- {short} (V1)
*
* SYNOPSIS
*   struct lfr_param * lfr_param_copy(struct lfr_param *param)
*
* FUNCTION
*   Duplicate a filter parameter set.  Returns NULL if out of memory.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

struct lfr_param * Liblfr_param_copy(UNUSED struct MyLibBase *base, struct lfr_param *param)
{
    return lfr_param_copy(param);
}



/****** fresample.library/lfr_param_seti ************************************
*
* NAME
*   lfr_param_seti -- {short} (V1)
*
* SYNOPSIS
*   void lfr_param_seti(struct lfr_param *param, lfr_param_t pname, int value)
*
* FUNCTION
*   Set an integer-valued parameter.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

void Liblfr_param_seti(UNUSED struct MyLibBase *base, struct lfr_param *param, lfr_param_t pname, int value)
{
    lfr_param_seti(param, pname, value);
}



/****** fresample.library/lfr_param_setf ************************************
*
* NAME
*   lfr_param_setf -- {short} (V1)
*
* SYNOPSIS
*   void lfr_param_setf(struct lfr_param *param, lfr_param_t pname, double value)
*
* FUNCTION
*   Set a float-valued parameter.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

void Liblfr_param_setf(UNUSED struct MyLibBase *base, struct lfr_param *param, lfr_param_t pname, double value)
{
    lfr_param_setf(param, pname, value);
}



/****** fresample.library/lfr_param_geti ************************************
*
* NAME
*   lfr_param_geti -- {short} (V1)
*
* SYNOPSIS
*   void lfr_param_geti(struct lfr_param *param, lfr_param_t pname, int *value)
*
* FUNCTION
*   Get the value of a parameter as an integer.  This will compute the
*   parameter value if necessary.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

void Liblfr_param_geti(UNUSED struct MyLibBase *base, struct lfr_param *param, lfr_param_t pname, int *value)
{
    lfr_param_geti(param, pname, value);
}



/****** fresample.library/lfr_param_getf ************************************
*
* NAME
*   lfr_param_getf -- {short} (V1)
*
* SYNOPSIS
*   void lfr_param_getf(struct lfr_param *param, lfr_param_t pname, double *value)
*
* FUNCTION
*   Get the value of a parameter as a floating-point number, or -1 if
*   the parameter is unset.  This will compute the parameter value if
*   necessary.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

void Liblfr_param_getf(UNUSED struct MyLibBase *base, struct lfr_param *param, lfr_param_t pname, double *value)
{
    lfr_param_getf(param, pname, value);
}



/****** fresample.library/lfr_info_name *************************************
*
* NAME
*   lfr_info_name -- {short} (V1)
*
* SYNOPSIS
*   const char * lfr_info_name(int pname)
*
* FUNCTION
*   Get the name of a filter info query, or return NULL if the info query
*   does not exist.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

const char * Liblfr_info_name(UNUSED struct MyLibBase *base, int pname)
{
    return lfr_info_name(pname);
}



/****** fresample.library/lfr_info_lookup ***********************************
*
* NAME
*   lfr_info_lookup -- {short} (V1)
*
* SYNOPSIS
*   int lfr_info_lookup(const char *pname, size_t len)
*
* FUNCTION
*   Get the index of a filter info query by name, or return -1 if the
*   info query does not exist.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

int Liblfr_info_lookup(UNUSED struct MyLibBase *base, const char *pname, size_t len)
{
    return lfr_info_lookup(pname, len);
}



/****** fresample.library/lfr_filter_new ************************************
*
* NAME
*   lfr_filter_new -- {short} (V1)
*
* SYNOPSIS
*   void lfr_filter_new(struct lfr_filter **fpp, struct lfr_param *param)
*
* FUNCTION
*   Create a low-pass filter with the given parameters.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

void Liblfr_filter_new(UNUSED struct MyLibBase *base, struct lfr_filter **fpp, struct lfr_param *param)
{
    lfr_filter_new(fpp, param);
}



/****** fresample.library/lfr_filter_free ***********************************
*
* NAME
*   lfr_filter_free -- {short} (V1)
*
* SYNOPSIS
*   void lfr_filter_free(struct lfr_filter *fp)
*
* FUNCTION
*   Free a low-pass filter.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

void Liblfr_filter_free(UNUSED struct MyLibBase *base, struct lfr_filter *fp)
{
    lfr_filter_free(fp);
}



/****** fresample.library/lfr_filter_delay **********************************
*
* NAME
*   lfr_filter_delay -- {short} (V1)
*
* SYNOPSIS
*   lfr_fixed_t lfr_filter_delay(const struct lfr_filter *fp)
*
* FUNCTION
*   Get the delay of a filter, in fixed point.  Filters are causal, so
*   you can subtract the filter delay from the position to create a
*   non-causal filter with zero delay.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

lfr_fixed_t Liblfr_filter_delay(UNUSED struct MyLibBase *base, const struct lfr_filter *fp)
{
    return lfr_filter_delay(fp);

}



/****** fresample.library/lfr_filter_geti ***********************************
*
* NAME
*   lfr_filter_geti -- {short} (V1)
*
* SYNOPSIS
*   void lfr_filter_geti(const struct lfr_filter *fp, int iname, int *value)
*
* FUNCTION
*   Query the filter and return an integer value.  This will cast if
*   necessary.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

void Liblfr_filter_geti(UNUSED struct MyLibBase *base, const struct lfr_filter *fp, int iname, int *value)
{
    lfr_filter_geti(fp, iname, value);
}



/****** fresample.library/lfr_filter_getf ***********************************
*
* NAME
*   lfr_filter_getf -- {short} (V1)
*
* SYNOPSIS
*   void lfr_filter_getf(const struct lfr_filter *fp, int iname, double *value)
*
* FUNCTION
*   Query the filter and return a floating-point value.  This will cast
*   if necessary.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

void Liblfr_filter_getf(UNUSED struct MyLibBase *base, const struct lfr_filter *fp, int iname, double *value)
{
    lfr_filter_getf(fp, iname, value);
}



/****** fresample.library/lfr_resample **************************************
*
* NAME
*   lfr_resample -- {short} (V1)
*
* SYNOPSIS
*   void lfr_resample(lfr_fixed_t *pos, lfr_fixed_t inv_ratio, unsigned *dither, int nchan, void *out, lfr_fmt_t outfmt, int outlen, const void *in, lfr_fmt_t infmt, int inlen, const struct lfr_filter *filter)
*
* FUNCTION
*   Resample an audio buffer.  Note that this function may need to
*   create intermediate buffers if there is no function which can
*   directly operate on the input and output formats.  No intermediate
*   buffers will be necessary if the following conditions are met:
*
*   - Input and output formats are identical.
*
*   - Sample format is either S16_NATIVE or F32_NATIVE.
*
*   - The number of channels is either 1 (mono) or 2 (stereo).
*
*   pos: Current position relative to the start of the input buffer,
*   expressed as a 32.32 fixed point number.  On return, this will
*   contain the updated position.  Positions outside the input buffer
*   are acceptable, it will be treated as if the input buffer were
*   padded with an unlimited number of zeroes on either side.
*
*   inv_ratio: Inverse of the resampling ratio, expressed as a 32.32
*   fixed point number.  This number is equal to the input sample rate
*   divided by the output sample rate.
*
*   dither: State of the PRNG used for dithering.
*
*   nchan: Number of interleaved channels.
*
*   out, in: Input and output buffers.  The buffers are not permitted to
*   alias each other.
*
*   outlen, inlen: Length of buffers, in frames.  Note that the length
*   type is 'int' instead of 'size_t'; this matches the precision of
*   buffer positions.
*
*   outfmt, infmt: Format of input and output buffers.
*
*   filter: A suitable low-pass filter for resampling at the given
*   ratio.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

void Liblfr_resample(UNUSED struct MyLibBase *base, lfr_fixed_t *pos, lfr_fixed_t inv_ratio, unsigned *dither, int nchan, void *out, lfr_fmt_t outfmt, int outlen, const void *in, lfr_fmt_t infmt, int inlen, const struct lfr_filter *filter)
{
    lfr_resample(pos, inv_ratio, dither, nchan, out, outfmt, outlen, in, infmt, inlen, filter);
}



/****** fresample.library/lfr_getcpufeatures ********************************
*
* NAME
*   lfr_getcpufeatures -- {short} (V2)
*
* SYNOPSIS
*   const struct lfr_cpuf * lfr_getcpufeatures(VOID)
*
* FUNCTION
*   Array of names for the CPU features this architecture supports.
*   Names are the lower case version of the flag names above, e.g.,
*   LFR_CPUF_ALTIVEC becomes "altivec".  Terminated by a zeroed entry.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

const struct lfr_cpuf * Liblfr_getcpufeatures(UNUSED struct MyLibBase *base)
{
    return lfr_getcpufeatures();
}



/****** fresample.library/lfr_setcpufeatures ********************************
*
* NAME
*   lfr_setcpufeatures -- {short} (V2)
*
* SYNOPSIS
*   unsigned lfr_setcpufeatures(unsigned flags)
*
* FUNCTION
*   Set which CPU features are allowed or disallowed.  This is primarily
*   used for comparing the performance and correctness of vector
*   implementations and scalar implementations.  It can also be used to
*   prohibit features that your CPU supports but which your OS does not.
*
*   Returns the CPU flags actually enabled, which will be the
*   intersection of the set of allowed flags (the argument) with the set
*   of features that the current CPU actually supports.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

unsigned Liblfr_setcpufeatures(UNUSED struct MyLibBase *base, unsigned flags)
{
    return lfr_setcpufeatures(flags);
}



/****** fresample.library/lfr_swap16 ****************************************
*
* NAME
*   lfr_swap16 -- {short} (V2)
*
* SYNOPSIS
*   void lfr_swap16(void *dest, const void *src, size_t count)
*
* FUNCTION
*   Swap the byte order on 16-bit data.  The destination can either be
*   the same buffer as the source, or it can be a non-overlapping
*   buffer.  Behavior is undefined if the two buffers partially overlap.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

void Liblfr_swap16(UNUSED struct MyLibBase *base, void *dest, const void *src, size_t count)
{
    lfr_swap16(dest, src, count);
}

