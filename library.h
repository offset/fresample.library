/*
   fresample.library internal library definitions.
   Generated with LibMaker 0.11.
*/

#ifndef FRESAMPLE_LIBRARY_H
#define FRESAMPLE_LIBRARY_H

#include <exec/libraries.h>
#include <exec/semaphores.h>


struct MyLibBase
{
	struct Library          LibNode;
	APTR                    Seglist;
	struct SignalSemaphore  BaseLock;
	BOOL                    InitFlag;
};

#endif      /* FRESAMPLE_LIBRARY_H */
